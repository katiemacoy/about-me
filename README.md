
# About me
* 👋 Hi, I'm Katie. I'm a senior product designer for the AI Assisted team. 
* I live in New Zealand but have Dutch and American citizenship.
* I love to cook. Right now I'm learning to cook authentic Chinese food.
* Another big passion is fitness. I do circus, ballet, HIIT, and more...
* I love to travel and take advantage of the GitLab remote lifestyle 😎
* I'm an [INFJ](https://www.16personalities.com/infj-personality)
* I'm a [growth seeker](https://principlesyou.com/archetypes/growthseeker)
* I'm [conscientious](https://www.discprofile.com/what-is-disc/disc-styles/conscientiousness)

# Timezone & working style
* I'm based in New Zealand (UTC +12)
* I prefer not to work in the evening, but I occassionally do it to speak with people in EMEA
* I generally prefer written communication as I like time to digest information. However, I think sync meetings are better when collaborating on something that's ambiguous or when giving or receiving critical feedback 
* I'm highly analytical and detail-oriented. This is a strength and a weakness :joy:

